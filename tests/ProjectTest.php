<?php

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/7/16
 * Time: 11:02 AM
 */
abstract class ProjectTest extends \Orchestra\Testbench\TestCase
{

    public function setUp()
    {
        parent::setUp();
        view()->addNamespace('smorken/controller-traited', __DIR__ . '/../views');
    }

    protected function getPackageProviders($app)
    {
        return array(
            'Smorken\ControllerTraited\ControllerServiceProvider',
        );
    }
}
