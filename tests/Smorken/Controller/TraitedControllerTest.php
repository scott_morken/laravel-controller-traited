<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/24/14
 * Time: 7:40 AM
 */

use Illuminate\Support\Facades\Route;
use Mockery as m;
use Smorken\ControllerTraited\TraitedController;

class TraitedControllerTest extends ProjectTest
{

    /**
     * @var m\Mock
     */
    protected $provider;

    public function setUp()
    {
        parent::setUp();

        $this->app['config']->set(
            'smorken/controller-traited::config.master',
            'smorken/controller-traited::testing.layout'
        );
        $this->provider = m::mock(CProvider::class);
        $this->app[CProvider::class] = $this->provider;
        $this->addRoutes();
    }

    protected function addRoutes()
    {
        Route::get('no-provider', 'TestControllerNoProvider@getIndex');
        Route::get('', 'TestController@getIndex');
        Route::post('', 'TestController@postIndex');
        Route::get('view/{id}', 'TestController@getView');
        Route::post('view/{id}', 'TestController@postView');
        Route::get('create', 'TestController@getCreate');
        Route::post('create', 'TestController@postCreate');
        Route::get('update/{id}', 'TestController@getUpdate');
        Route::post('update/{id}', 'TestController@postUpdate');
        Route::get('delete/{id}', 'TestController@getDelete');
        Route::delete('delete/{id}', 'TestController@deleteDelete');

        Route::get('no-package', 'TestControllerNoPackage@getIndex');
    }

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testControllerThrowsNoProvider()
    {
        $response = $this->get('/no-provider');
        $response->assertSee('A provider must be set to use this controller.');
        $response->assertStatus(500);
    }

    public function testGetIndex()
    {
        $this->provider->shouldReceive('all')->once()->andReturn([]);
        $response = $this->get('/');
        echo $response->getContent();
        $response->assertSuccessful();
    }

    public function testGetView()
    {
        $model = $this->mockSimple();
        $this->provider->shouldReceive('find')
                       ->once()
                       ->with(1)
                       ->andReturn($model);
        $response = $this->get('view/1');
        $response->assertSuccessful();
    }

    public function mockSimple()
    {
        $mocked = m::mock('StdClass');
        return $mocked;
    }

    public function testGetIndexThrowsViewNotFound()
    {
        $this->provider->shouldReceive('all')->andReturn([]);
        $response = $this->get('no-package');
        $response->assertStatus(500);
        $response->assertSee('InvalidArgumentException');
        $response->assertSee('View [.index] not found');
    }

    public function testGetCreate()
    {
        $this->provider->shouldReceive('getModel')
                       ->once()
                       ->andReturn($this->mockSimple());
        $response = $this->get('create');
        $response->assertSee('Create');
        $response->assertSee('Form');
        $response->assertSuccessful();
    }

    public function testPostCreate()
    {
        $model = $this->mockSimple();
        $provider = $this->provider;
        $provider->shouldReceive('create')->with(['foo' => 'bar', 'fiz' => 'buz'])->andReturn($model);
        $provider->shouldReceive('setModel')->with($model);
        $provider->shouldReceive('getModel')
                 ->once()
                 ->andReturn($model);
        $provider->shouldReceive('errors')
                 ->once()
                 ->andReturn(false);
        $provider->shouldReceive('name')
                 ->once()
                 ->with($model)
                 ->andReturn('foo');
        $response = $this->post('create', ['foo' => 'bar', 'fiz' => 'buz']);
        $response->assertRedirect('');
        $response->assertSessionHas('success');
    }

    public function testPostCreateWithErrors()
    {
        $msgbag = m::mock('MsgBag');
        $provider = $this->provider;
        $provider->shouldReceive('create')->with(['foo' => 'bar', 'fiz' => 'buz'])->andReturn(false);
        $provider->shouldReceive('setModel')->with(false);
        $provider->shouldReceive('getModel')
                 ->once()
                 ->andReturn(false);
        $provider->shouldReceive('errors')
                 ->twice()
                 ->andReturn($msgbag);
        $response = $this->post('create', ['foo' => 'bar', 'fiz' => 'buz']);
        $response->assertSessionHasErrors();
        $response->assertRedirect('create');
    }

    public function testGetUpdate()
    {
        $model = $this->mockSimple();
        $provider = $this->provider;
        $provider->shouldReceive('find')
                 ->once()
                 ->with(1)
                 ->andReturn($model);
        $provider->shouldReceive('getModel')
                 ->never();
        $response = $this->get('update/1');
        $response->assertSuccessful();
        $response->assertSee('Update')
                 ->assertSee('Form');
    }

    public function testLoadModelNotFoundIs404()
    {
        $provider = $this->provider;
        $provider->shouldReceive('find')
                 ->once()
                 ->with(1)
                 ->andReturnNull();
        $provider->shouldReceive('getModel')
                 ->never();
        $response = $this->get('update/1');
        $response->assertStatus(404)
                 ->assertSee('Sorry, the page you are looking for could not be found');
    }

    public function testPostUpdate()
    {
        $model = $this->mockSimple();
        $provider = $this->provider;
        $provider->shouldReceive('find')
                 ->once()
                 ->with(1)
                 ->andReturn($model);
        $provider->shouldReceive('update')->with($model, ['foo' => 'bar', 'fiz' => 'buz']);
        $provider->shouldReceive('setModel')->with($model);
        $provider->shouldReceive('getModel')
                 ->once()
                 ->andReturn($model);
        $provider->shouldReceive('errors')
                 ->once()
                 ->andReturn(false);
        $provider->shouldReceive('name')
                 ->once()
                 ->with($model)
                 ->andReturn('foo');
        $response = $this->post('update/1', ['foo' => 'bar', 'fiz' => 'buz']);
        $response->assertSessionHas('success')
                 ->assertRedirect('');
    }

    public function testPostUpdateWithErrors()
    {
        $msgbag = m::mock('MessageBag');
        $model = $this->mockSimple();
        $provider = $this->provider;
        $provider->shouldReceive('find')
                 ->once()
                 ->with(1)
                 ->andReturn($model);
        $provider->shouldReceive('update')->with($model, ['foo' => 'bar', 'fiz' => 'buz']);
        $provider->shouldReceive('setModel')->with($model);
        $provider->shouldReceive('getModel')
                 ->once()
                 ->andReturn($model);
        $provider->shouldReceive('errors')
                 ->twice()
                 ->andReturn($msgbag);
        $provider->shouldReceive('id')
                 ->once()
                 ->with($model)
                 ->andReturn('1');
        $response = $this->post('update/1', ['foo' => 'bar', 'fiz' => 'buz']);
        $response->assertSessionHasErrors()
                 ->assertRedirect('update/1');
    }

    public function testGetDelete()
    {
        $model = $this->mockSimple();
        $provider = $this->provider;
        $provider->shouldReceive('find')
                 ->once()
                 ->with(1)
                 ->andReturn($model);
        $provider->shouldReceive('getModel')
                 ->never();
        //$this->sut->setBase('smorken/controller-traited::testing');
        $response = $this->get('delete/1');
        $response->assertSuccessful()
                 ->assertSee('Delete');
    }

    public function testDeleteDelete()
    {
        $model = $this->mockSimple();
        $provider = $this->provider;
        $provider->shouldReceive('find')
                 ->once()
                 ->with(1)
                 ->andReturn($model);
        $provider->shouldReceive('delete')
                 ->once()
                 ->with($model)
                 ->andReturn(true);
        $provider->shouldReceive('getModel')->never();
        $provider->shouldReceive('find')->never();
        $response = $this->delete('delete/1');
        $response->assertSessionHas('success')
                 ->assertRedirect('');
    }

    public function testDeleteDeleteFails()
    {
        $model = $this->mockSimple();
        $provider = $this->provider;
        $provider->shouldReceive('find')
                 ->once()
                 ->with(1)
                 ->andReturn($model);
        $provider->shouldReceive('delete')
                 ->once()
                 ->with($model)
                 ->andReturn(false);
        $provider->shouldReceive('getModel')->never();
        $provider->shouldReceive('find')->never();
        $response = $this->delete('delete/1');
        $response->assertSessionHas('danger')
                 ->assertRedirect('');
    }

    public function testGetOpsStyle()
    {
        $this->assertEquals('list-unstyled', TestController::opsStyle());
    }

    public function testGetViewName()
    {
        $sut = new TestController($this->provider);
        $r = new \ReflectionClass($sut);
        $props = $r->getDefaultProperties();
        $this->assertEquals('test', $props['subnav']);
        $this->assertEquals('smorken/controller-traited::testing.index', $sut->viewName('index'));
    }
}

class CProvider
{

}

class TestControllerNoProvider extends TraitedController
{

}

class TestController extends TraitedController
{

    protected $subnav = 'test';

    protected $base = 'testing';

    protected $package = 'smorken/controller-traited::';

    public function __construct(CProvider $provider)
    {
        $this->setProvider($provider);
        parent::__construct();
    }

    public function postCreate()
    {
        return $this->postCreateDefault(['foo' => 'bar', 'fiz' => 'buz']);
    }

    public function postUpdate($id)
    {
        return $this->postUpdateDefault($id, ['foo' => 'bar', 'fiz' => 'buz']);
    }

    public function setPaged($v)
    {
        $this->paged = $v;
    }

    public function viewName($view = null)
    {
        return $this->getViewName($view);
    }
}

class TestControllerNoPackage extends \Smorken\ControllerTraited\BaseController
{

    use \Smorken\ControllerTraited\Traits\Crud, \Smorken\ControllerTraited\Traits\Index, \Smorken\ControllerTraited\Traits\Provider;

    protected $package = null;

    public function __construct(CProvider $provider)
    {
        $this->setProvider($provider);
        parent::__construct();
    }
}
