<h3>The following record(s) will be deleted:</h3>
<div class="well">
    {{ $model }}
</div>
<form method="post">
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="button-group">
        <button type="submit" name="Delete" class="btn btn-danger">Delete</button>
        <a href="{{ action($controller . '@getIndex') }}" title="Cancel delete" class="btn btn-primary">Cancel</a>
    </div>
</form>
