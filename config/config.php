<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/24/14
 * Time: 7:00 AM
 */
return array(
    'master' => 'smorken/views::layouts.master',
    'icons' => array(
        'view' => 'glyphicon glyphicon-eye-open fa fa-eye',
        'update' => 'glyphicon glyphicon-pencil fa fa-pencil',
        'delete' => 'glyphicon glyphicon-trash fa fa-trash',
    )
);
