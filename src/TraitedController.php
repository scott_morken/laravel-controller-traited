<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/7/16
 * Time: 10:56 AM
 */

namespace Smorken\ControllerTraited;

use Smorken\ControllerTraited\Traits\Crud;
use Smorken\ControllerTraited\Traits\Index;
use Smorken\ControllerTraited\Traits\Ops;
use Smorken\ControllerTraited\Traits\Provider;

class TraitedController extends BaseController
{

    use Crud, Index, Ops, Provider;
}
