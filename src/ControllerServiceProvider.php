<?php namespace Smorken\ControllerTraited;

use Illuminate\Support\ServiceProvider;

class ControllerServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../views/common', 'smorken/controller-traited');
        $this->bootConfig();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }

    protected function bootConfig()
    {
        $package_path = __DIR__ . '/../config/config.php';
        $app_path = config_path('controller-traited.php');
        $this->mergeConfigWith($package_path, $app_path, 'smorken/controller-traited::config');
        $this->publishes([$package_path => $app_path], 'config');
    }

    protected function mergeConfigWith($package_path, $app_path, $key)
    {
        if (file_exists($app_path)) {
            $app_config = require $app_path;
        } else {
            $app_config = [];
        }
        $package_config = require $package_path;
        $this->app['config']->set($key, array_merge($package_config, $app_config));
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
