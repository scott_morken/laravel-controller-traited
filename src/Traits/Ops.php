<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/7/16
 * Time: 10:48 AM
 */

namespace Smorken\ControllerTraited\Traits;

trait Ops
{

    public static function ops($id = null)
    {
        $ops = [
            'Create' => ['url' => action(static::getRoute('getCreate'))],
            'List'   => ['url' => action(static::getRoute('getIndex'))],
        ];
        if ($id) {
            $ops = array_merge($ops, static::opsWithId($id));
        }
        return $ops;
    }

    public static function opsWithId($id, $show_text = true)
    {
        return [
            'View'   => [
                'url'       => action(static::getRoute('getView'), ['id' => $id]),
                'icon'      => config('smorken/controller-traited::config.icons.view', 'glyphicon glyphicon-eye-open'),
                'show_text' => $show_text,
            ],
            'Update' => [
                'url'       => action(static::getRoute('getUpdate'), ['id' => $id]),
                'icon'      => config('smorken/controller-traited::config.icons.update', 'glyphicon glyphicon-pencil'),
                'show_text' => $show_text,
            ],
            'Delete' => [
                'url'       => action(static::getRoute('getDelete'), ['id' => $id]),
                'icon'      => config('smorken/controller-traited::config.icons.delete', 'glyphicon glyphicon-trash'),
                'show_text' => $show_text,
            ],
        ];
    }

    public static function opsStyle()
    {
        return 'list-unstyled';
    }
}
