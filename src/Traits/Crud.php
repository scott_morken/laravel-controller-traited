<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/7/16
 * Time: 10:43 AM
 */

namespace Smorken\ControllerTraited\Traits;

trait Crud
{

    public function getView($id)
    {
        return $this->handleCommonGets('view', true, $id);
    }

    protected function handleCommonGets($action, $idreq = true, $id = null)
    {
        if ($idreq || $id) {
            $model = $this->loadModel($id);
        } else {
            $model = $this->getProvider()->getModel();
        }
        $this->handleOps($id);
        return view($this->getViewName($action, true))
            ->with('model', $model)
            ->with('base', $this->getViewName());
    }

    public function getCreate()
    {
        return $this->handleCommonGets('create', false);
    }

    public function getUpdate($id)
    {
        return $this->handleCommonGets('update', true, $id);
    }

    public function getDelete($id)
    {
        return $this->handleCommonGets('delete', true, $id);
    }

    public function deleteDelete($id)
    {
        if ($this->getProvider()->delete($this->loadModel($id))) {
            session()->flash('success', "Resource with id #$id deleted.");
        } else {
            session()->flash('danger', $id . ' NOT deleted.');
        }
        return redirect()->action($this->getRoute('getIndex'));
    }

    protected function postCreateDefault(array $attributes)
    {
        $model = $this->getProvider()->create($attributes);
        $this->getProvider()->setModel($model);
        return $this->handlePostSaveRedirects($this->getProvider());
    }

    public function handlePostSaveRedirects($provider)
    {
        $model = $provider->getModel();
        if (!$provider->errors()) {
            session()->flash('success', $provider->name($model) . ' saved.');
            return redirect()->action($this->getRoute('getIndex'));
        }
        if ($model) {
            return redirect()->action($this->getRoute('getUpdate'), ['id' => $provider->id($model)])
                             ->withInput()
                             ->withErrors($provider->errors());
        } else {
            return redirect()->action($this->getRoute('getCreate'))
                             ->withInput()
                             ->withErrors($provider->errors());
        }
    }

    protected function postUpdateDefault($id, array $attributes)
    {
        $model = $this->loadModel($id);
        $this->getProvider()->update($model, $attributes);
        $this->getProvider()->setModel($model);
        return $this->handlePostSaveRedirects($this->getProvider());
    }
}
