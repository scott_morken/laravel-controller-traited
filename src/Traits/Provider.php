<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/7/16
 * Time: 10:49 AM
 */

namespace Smorken\ControllerTraited\Traits;

use Smorken\ControllerTraited\ControllerException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait Provider
{

    /**
     * @var object provider for controller
     */
    protected $provider;

    public function loadModel($id)
    {
        $model = $this->getProvider()->find($id);
        if (!$model) {
            throw new NotFoundHttpException("Resource could not be located.");
        }
        return $model;
    }

    /**
     * @throws ControllerException
     * @return object
     */
    protected function getProvider()
    {
        if (!$this->provider) {
            throw new ControllerException('A provider must be set to use this controller.');
        }
        return $this->provider;
    }

    /**
     * @param $provider
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
    }
}
