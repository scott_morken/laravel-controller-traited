<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/7/16
 * Time: 10:42 AM
 */

namespace Smorken\ControllerTraited\Traits;

trait Index
{

    /**
     * @var bool $paged use paged index or standard index
     */
    protected $paged = false;

    public function getIndex()
    {
        if ($this->paged) {
            return $this->getIndexPaged();
        } else {
            return $this->getIndexAll();
        }
    }

    protected function getIndexPaged()
    {
        $models = $this->getProvider()->paginate();
        $this->handleOps();
        return view($this->getViewName('index'))
            ->with('models', $models);
    }

    protected function getIndexAll()
    {
        $models = $this->getProvider()->all();
        $this->handleOps();
        return view($this->getViewName('index'))
            ->with('models', $models);
    }
}
