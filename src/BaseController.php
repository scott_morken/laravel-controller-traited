<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/24/14
 * Time: 6:58 AM
 */

namespace Smorken\ControllerTraited;

use Illuminate\Routing\Controller;

class BaseController extends Controller
{

    /**
     * @var string master layout template
     */
    protected $master;
    /**
     * @var string package name that goes with $base views
     */
    protected $package;
    /**
     * @var string base path name of view that controller uses ('base')
     */
    protected $base = '';
    /**
     * @var string Subnav lookup string
     */
    protected $subnav = '';

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $this->setupMaster();
        $this->initSubnav();
        $this->addController();
    }

    /**
     * Setup the master template.
     *
     * @return void
     */
    protected function setupMaster()
    {
        if (!$this->master) {
            $this->loadMasterFromConfig();
        }
        if ($this->master) {
            view()->share('master', $this->master);
        }
    }

    protected function loadMasterFromConfig()
    {
        if ($this->package) {
            $subpath = $this->package . 'config.master';
            $l = config($subpath, null);
            if ($l) {
                $this->setMaster($l);
                return true;
            }
        }
        $l = config('smorken/controller-traited::config.master', 'layouts.master');
        if ($l) {
            $this->setMaster($l);
            return true;
        }
        return false;
    }

    /**
     * @param string $layout
     */
    public function setMaster($layout)
    {
        $this->master = $layout;
    }

    protected function initSubnav()
    {
        if ($this->subnav) {
            view()->share('sub', $this->subnav);
        }
    }

    protected function addController()
    {
        view()->share('controller', get_called_class());
    }

    public static function getRoute($action)
    {
        $called = get_called_class();
        if (0 !== strpos($called, '\\')) {
            $called = '\\' . $called;
        }
        return $called . '@' . $action;
    }

    /**
     * @param string $base
     */
    public function setBase($base)
    {
        $this->base = $base;
    }

    /**
     * @param string $package
     */
    public function setPackage($package)
    {
        $this->package = $package;
    }

    /**
     * @param string $subnav
     */
    public function setSubnav($subnav)
    {
        $this->subnav = $subnav;
        $this->initSubnav();
    }

    protected function getViewName($view = null)
    {
        $viewname = '';
        if ($this->package) {
            $viewname = $this->package;
        }
        $viewname .= $this->base;
        if (substr($viewname, -1) !== '.' && $view) {
            $viewname .= '.';
        }
        return $viewname . $view;
    }

    protected function handleOps($id = null)
    {
        if (method_exists(get_called_class(), 'ops')) {
            view()->share('operations', static::ops($id));
        }
    }

    /**
     * Parse the given filter and options.
     *
     * @param  \Closure|string $filter
     * @param  array $options
     * @return array
     */
    protected function parseFilter($filter, array $options)
    {
        $original = parent::parseFilter($filter, $options);
        if (isset($options['parameters'])) {
            $original['parameters'] = array_merge($original['parameters'], $options['parameters']);
        }
        return $original;
    }
}
